package com.manhnd.userservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "profiles")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Profile implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private String email;
	private String name;
	private String status;
	private String role;
	
}
