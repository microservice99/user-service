package com.manhnd.userservice.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.Data;

@Table (name = "users")
@Entity
@Data
//@Builder
public class User implements UserDetails {

	private static final long serialVersionUID = 3592549577903104696L;
	@Id
	private String ids;
	private String address;
	private String authoritynames;
	private String email;
	private String firstname;
	private String lastname;
	private Integer phonenumber;
	private String username;

	@JsonIgnore
	private String password;	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<String> authority = new ArrayList<>();
		authority.add(authoritynames);
		List<GrantedAuthority> list = authority
				.stream()
				.map(auth -> new SimpleGrantedAuthority(auth))
				.collect(Collectors.toList());

		return list;
	}
	
}

