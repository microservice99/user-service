package com.manhnd.userservice.dto;

import lombok.Data;

@Data
public class UserDTO {

	private String ids;
	private String username;
	private String password;
	private String employeeId;
	private String token;
//	private String refreshtoken;
}
