package com.manhnd.userservice.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> implements Serializable{
	private static final long serialVersionUID = 1L;
	private int code;
	private String message;
	private T data;
}

