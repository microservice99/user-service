package com.manhnd.userservice.dto;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class AccessTokenResponse {
	@SerializedName("access_token")
    private String accessToken;

    @SerializedName("expires_in")
    private long expiresIn;

    @SerializedName("refresh_expires_in")
    private long refreshExpiresIn;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("id_token")
    private String idToken;

    @SerializedName("not-before-policy")
    private int notBeforePolicy;

    @SerializedName("scope")
    private String scope;

   
}
