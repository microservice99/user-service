package com.manhnd.userservice.event;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.manhnd.userservice.config.ConfigProducer;
import com.manhnd.userservice.model.Profile;
import com.manhnd.userservice.model.User;
import com.manhnd.userservice.repository.ProfileRepository;
import com.manhnd.userservice.service.ProfileService;
import com.manhnd.userservice.service.UserService;

import lombok.extern.slf4j.Slf4j;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

@Service
@Slf4j
public class EventConsumer {
	
	private static final Logger logger = LoggerFactory.getLogger(EventConsumer.class);
	
    @Autowired
    ConfigProducer configProducer;
    
    @Autowired
    ProfileService profileService;
    
    @Autowired
    ProfileRepository profileRepository;
    
    public EventConsumer(ReceiverOptions<String, String> receiverOptions) {
        KafkaReceiver.create(receiverOptions.subscription(Collections.singleton("manhnd99")))
            .receive()
            .doOnNext(this::processUserTopic)
            .doOnError(error -> {
                // Log lỗi để theo dõi
                logger.error("Error while processing Kafka message: {}", error.getMessage());
            })
            .retry() // Tự động retry nếu gặp lỗi
            .subscribe();
    }


    public String getValueInOffet(String topicName, Integer partitionNumber, Integer offsetNumber, String key) {
        String value = null;
        if (configProducer != null) {
            Properties props = configProducer.kafkaConsumer();
            KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
            TopicPartition topicPartition = new TopicPartition(topicName, partitionNumber);
            consumer.assign(Collections.singletonList(topicPartition));
            consumer.seekToBeginning(Collections.singletonList(topicPartition));
            // start lấy message mới nhất
//            consumer.seekToEnd(Collections.singletonList(topicPartition));
//            consumer.poll(Duration.ZERO);
//            long latestOffset = consumer.position(topicPartition) - 1;
//            consumer.seek(topicPartition, latestOffset); 
            // end lấy message mới nhất
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
            for (ConsumerRecord<String, String> record : records) {
                if (record.offset() == offsetNumber && key.equals(record.key())) {
                    value = record.value();
                    break;
                }
            	value = record.value();
            }
            consumer.close();
        }
        return value;
    }
    
    public void processUserTopic(ReceiverRecord<String, String> record) {
    	String data = getValueInOffet("manhnd99", 0 , 1, "ManhND99");
    	String data1 = getValueInOffet("user",0,1,"");
    	System.out.println(data1);
    	Gson gson = new Gson();
    	User _user = gson.fromJson(data, User.class);
    	Profile profile = new Profile();
    	profile.setId(_user.getIds());
    	profile.setEmail(_user.getEmail());
    	profile.setName(_user.getUsername());
    	profile.setRole(_user.getAuthoritynames());
    	profile.setStatus("1");
//    	profileRepository.save(profile);
    	profileService.saveProfile(profile);
    }
}
