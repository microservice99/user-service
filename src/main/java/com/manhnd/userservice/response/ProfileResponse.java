package com.manhnd.userservice.response;

import java.io.Serializable;
import java.util.List;

import com.manhnd.userservice.model.Profile;

import lombok.Data;

@Data
public class ProfileResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<Profile> lstProfile;
	private String status;
	private Integer page;
	private Integer size;
	private Integer total;
}
