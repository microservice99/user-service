package com.manhnd.userservice.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.manhnd.userservice.model.Profile;
import com.manhnd.userservice.response.ProfileResponse;

public interface ProfileService {

	void saveProfiles(List<Profile> profiles);
	
	ProfileResponse getProfile(String id, Pageable pageable);
	
	ProfileResponse getAllProfiles();
	
	void saveProfile(Profile profile);
}
