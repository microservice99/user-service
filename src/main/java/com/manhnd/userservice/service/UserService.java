package com.manhnd.userservice.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.manhnd.userservice.model.User;

import reactor.core.publisher.Mono;

public interface UserService {
	
	Mono<String> testTopic(String str);
	
	List<User> getAllUsers(String username, Pageable pageable);
	
	void saveUsers(User user);
	
	

//	boolean checkUserName(String username);
//	void saveTokenWithUserName(String username, String token);
}
