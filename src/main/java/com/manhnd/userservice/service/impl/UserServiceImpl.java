package com.manhnd.userservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.manhnd.userservice.event.EventProducer;
import com.manhnd.userservice.model.User;
import com.manhnd.userservice.repository.UserRepository;
import com.manhnd.userservice.service.UserService;
import com.manhnd.userservice.util.DataUtil;

import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepo;
    @Autowired
    
    EventProducer eventProducer;
    
    @Autowired
    EntityManager entityManager;

	@Override
	public Mono<String> testTopic(String str) {
//		eventProducer.send("test-topic", 1, str, "Nguyen Dang Manh").subscribe();
		return Mono.just(str) ;
	}

	@Override
	public List<User> getAllUsers(String username, Pageable pageable) {
		List<User> listUser = new ArrayList<User>();
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT username FROM users WHERE 1=1");
			if(StringUtils.hasText(username)) {
				str.append(" AND username LIKE ?");
			}
			str.append(" ORDER BY ids ASC");
			str.append(" LIMIT ? OFFSET ?");
			Query query = entityManager.createNativeQuery(str.toString());
			int paramIndex = 1;
			if (StringUtils.hasText(username)) {
				query.setParameter(paramIndex++, "%" + username + "%");
			}
			query.setParameter(paramIndex++, pageable.getPageSize());
			query.setParameter(paramIndex, pageable.getPageNumber() * pageable.getPageSize());
			List<Object[]> lstData = query.getResultList();
			if (!DataUtil.isNullOrEmpty(lstData)) {
				lstData.forEach(object -> {
					User user = new User();
					user.setAddress(DataUtil.safeToString(object[0]));
					listUser.add(user);
				});
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		return listUser;
	}

	@Override
	@Transactional
	public void saveUsers(User user) {
		User _user = new User();
		_user.setAddress(user.getAddress());
		_user.setAuthoritynames(user.getAuthoritynames());
		_user.setEmail(user.getEmail());
		_user.setFirstname(user.getFirstname());
		_user.setLastname(user.getLastname());
		_user.setIds(user.getIds());
		_user.setPassword(user.getPassword());
		_user.setUsername(user.getUsername());
		_user.setPhonenumber(user.getPhonenumber());
		userRepo.save(_user);
		Gson gson = new Gson();
		String json = gson.toJson(_user);
		eventProducer.send("user-topic", 0, json, "users").subscribe();
	}



//	@Override
//	public boolean checkUserName(String username) {
//		User _user = userRepo.findByUsername(username);
//		if (_user != null) {
//			return true;
//		}
//		return false;
//	}

//	@Override
//	public void saveTokenWithUserName(String username, String token) {
//		User _user = userRepo.findByUsername(username);
//		_user.setAccesstoken(token);
//		userRepo.save(_user);
//	}

}
