package com.manhnd.userservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.manhnd.userservice.model.Profile;
import com.manhnd.userservice.repository.ProfileRepository;
import com.manhnd.userservice.response.ProfileResponse;
import com.manhnd.userservice.service.ProfileService;
import com.manhnd.userservice.util.DataUtil;
import com.manhnd.userservice.util.RedisHelper;

@Service
public class ProfileServiceImpl implements ProfileService{
	
	@Autowired
	private ProfileRepository profileRepository;
	
    @Autowired
    EntityManager entityManager;
    
	@Autowired
	private RedisHelper redisHelper;
	
	// save profile in queue
	@Override
	public void saveProfiles(List<Profile> profiles) {
		for (Profile profile: profiles) {
			Profile _profile = new Profile();
			_profile.setEmail(profile.getEmail());
			_profile.setId(profile.getId());
			_profile.setName(profile.getName());
			_profile.setRole(profile.getRole());
			_profile.setStatus(profile.getStatus());
			redisHelper.pushToQueue(_profile);
			processSaveProfile();
		}
		
	}
	
	// process profile in queue
	@Scheduled(fixedRate = 5000)
	public void processSaveProfile() {
		Profile profile = Profile.class.cast(redisHelper.popFromQueue());
		while(profile != null) {
			profileRepository.save(profile);
			profile = Profile.class.cast(redisHelper.popFromQueue());
		}
	}
	
	private String genKeyRedis(String id, Pageable pageable) {
		String key = String.format("profile=%s&pageNumber=%d&pageSize=%d",id, pageable.getPageNumber(), pageable.getPageSize());
		return key;
	}

	@SuppressWarnings("unchecked")
	@Override
//	@PreAuthorize("hasAuthority('ADMIN')")
	public ProfileResponse getProfile(String id, Pageable pageable) {
		ProfileResponse resp = new ProfileResponse();
		String key = genKeyRedis(id,pageable);
		Object response = redisHelper.getKey(key);
		if (response == null) {
			List<Profile> listUser = new ArrayList<Profile>();
			try {
				StringBuilder str = new StringBuilder();
				str.append("SELECT * FROM profiles WHERE 1=1");
				if(StringUtils.hasText(id)) {
					str.append(" AND id LIKE ?");
					str.append(" OR name LIKE ?");
				}

				str.append(" ORDER BY id ASC");
				str.append(" LIMIT ? OFFSET ?");
				Query query = entityManager.createNativeQuery(str.toString()); 
				int paramIndex = 1;
				if (StringUtils.hasText(id)) {
					query.setParameter(paramIndex++, "%" + id + "%");
					query.setParameter(paramIndex++, "%" + id + "%");
				}
				query.setParameter(paramIndex++, pageable.getPageSize());
				query.setParameter(paramIndex++, (pageable.getPageNumber()-1)*pageable.getPageSize());
				List<Object[]> lstData = query.getResultList();
				if (!DataUtil.isNullOrEmpty(lstData)) {
					lstData.forEach(object -> {
						Profile user = new Profile();
						user.setId(DataUtil.safeToString(object[0]));
						user.setEmail(DataUtil.safeToString(object[1]));
						user.setName(DataUtil.safeToString(object[2]));
						user.setStatus(DataUtil.safeToString(object[3]));
						user.setRole(DataUtil.safeToString(object[4]));
						listUser.add(user);
					});
				}
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
			}


			resp.setLstProfile(listUser);
			resp.setStatus("Susscess");
			resp.setPage(pageable.getPageNumber());
			resp.setSize(pageable.getPageSize());
			resp.setTotal(getTotalRecord(id));
			redisHelper.setKey(key, resp);
			redisHelper.setKeyTest("test", "Manh dang test");
		}
		else {
			resp = ProfileResponse.class.cast(response);
		}
		return resp;
	}
	
	@SuppressWarnings("unchecked")
	private Integer getTotalRecord(String id) {
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT COUNT(*) AS total FROM profiles WHERE 1=1");
			if(StringUtils.hasText(id)) {
				str.append(" AND id LIKE ?");
				str.append(" OR name LIKE ?");
			}
			Query query = entityManager.createNativeQuery(str.toString());
			int paramIndex = 1;
			if (StringUtils.hasText(id)) {
				query.setParameter(paramIndex++, "%" + id + "%");
				query.setParameter(paramIndex++, "%" + id + "%");
			}
			List<Object[]> lstData = query.getResultList();
			return DataUtil.safeToInt(lstData.get(0));
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
			return 0;
			
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ProfileResponse getAllProfiles() {
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT pro.*, infor.address FROM profiles as pro");
			str.append(" INNER JOIN information as infor ON pro.id = infor.id");
			Query query = entityManager.createNativeQuery(str.toString());
			List<Object[]> lstData = query.getResultList();
			System.out.println(lstData);
		}catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	@Override
	public void saveProfile(Profile profile) {
		Profile _entity = new Profile();
		_entity.setEmail(profile.getEmail());
		_entity.setId(profile.getId());
		_entity.setName(profile.getName());
		_entity.setRole(profile.getRole());
		_entity.setStatus(profile.getStatus());
		profileRepository.save(_entity);
	}
	

}
