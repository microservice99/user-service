package com.manhnd.userservice.service.impl;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.manhnd.userservice.dto.AccessTokenResponse;
import com.manhnd.userservice.dto.DataVerify;
import com.manhnd.userservice.dto.ResponseDto;
import com.manhnd.userservice.model.Information;
import com.manhnd.userservice.repository.InformationRepository;
import com.manhnd.userservice.service.InformationService;
import com.manhnd.userservice.util.RedisHelper;
import com.manhnd.userservice.util.RestHelper;

@Service
public class InformationServiceImpl implements InformationService{
	
	@Autowired
	private RedisHelper redisHelper;
	
	@Autowired
	private RestHelper restHelper;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private InformationRepository informationRepository;

	@Override
	public void saveInformation(List<Information> lstInfor) {
		Information _entity = new Information();
		for (Information item: lstInfor) {
			_entity.setId(item.getId());
			_entity.setAddress(item.getAddress());
			_entity.setOld(item.getOld());
			_entity.setBirthday(item.getBirthday());
			redisHelper.pushToQueue(_entity);
			processQueueInformation();
		}
	}
	
	public void processQueueInformation() {
		Information infor = Information.class.cast(redisHelper.popFromQueue());
		while(infor != null) {
			informationRepository.save(infor);
			infor = Information.class.cast(redisHelper.popFromQueue());
		}
	}

	@Override
	public void getDataOtherSystem() {
		long start = System.currentTimeMillis();
		String tk = "";
		try {
			DataVerify queue = redisHelper.dequeue("DATA_VERIFY_QUEUE", DataVerify.class);
			long currentDateMillis = System.currentTimeMillis();

			if (queue == null || (queue.getExp() - currentDateMillis <= 480000)) {
			    HttpHeaders headers = new HttpHeaders();
			    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			    String URL_LOGIN = "http://localhost:8080/realms/manhnguyen99/protocol/openid-connect/token";

			    MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
			    requestBody.add("grant_type", "client_credentials");
			    requestBody.add("client_id", "manhnguyen99_keycloak");
			    requestBody.add("client_secret", "1YfJRgKVFWvu1AHEnJdgXw044Osazrxx"); // Lấy từ biến môi trường
			    requestBody.add("scope", "openid");

			    HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(requestBody, headers);
			    ResponseEntity<String> response = restTemplate.exchange(URL_LOGIN, HttpMethod.POST, requestEntity, String.class);
			    
			    Gson gson = new Gson();
			    AccessTokenResponse res = gson.fromJson(response.getBody(), AccessTokenResponse.class);
			    
			    long exp = currentDateMillis + (res.getExpiresIn() * 1000);
			    DataVerify data = DataVerify.builder().tk(res.getAccessToken()).exp(exp).build();
			    redisHelper.pushLeftPushQueue("DATA_VERIFY_QUEUE", data);
			    
			    // Gọi API GET với token mới
			    HttpHeaders headersOthter = new HttpHeaders();
			    headersOthter.set("Authorization", "Bearer " + res.getAccessToken());
			    HttpEntity<String> entity = new HttpEntity<>(headersOthter);
			    
			    String API_GETDATA = "http://localhost:8088/api/hello";
			    ResponseEntity<String> responses = restTemplate.exchange(API_GETDATA, HttpMethod.GET, entity, String.class);
			    System.out.println(responses.getBody());
			}

			
		}catch (Exception ex) {
			
		}
		
	}
	
	

}
