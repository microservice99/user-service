package com.manhnd.userservice.service;

import java.util.List;

import com.manhnd.userservice.model.Information;

public interface InformationService {

	void saveInformation(List<Information> lstInfor);
	
	void getDataOtherSystem();
}
