package com.manhnd.userservice.controller;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.TransformerSupplier;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.manhnd.userservice.event.EventProducer;
import com.manhnd.userservice.model.User;
import com.manhnd.userservice.request.JwtRequest;
import com.manhnd.userservice.response.JwtResponse;
import com.manhnd.userservice.service.UserService;
import com.manhnd.userservice.util.JwtTokenUtil;
import com.manhnd.userservice.util.QueryStateStore;
import com.manhnd.userservice.util.StateStoreTransformer;

import lombok.extern.slf4j.Slf4j;


@RestController
@Slf4j
@RequestMapping("/api/v1/users")
public class JwtAuthenticationController {
	
	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationController.class);

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private KafkaStreams streams;
	
	@Autowired
	UserService userService;

	@Autowired
    EventProducer eventProducer;
	
	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;
	
	@Value("${spring.kafka.consumer.group-id}")
	private String consumerGroupId;
	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		logger.info("JwtRequest body: {}", authenticationRequest.getUsername());
		logger.info("Susscess");
//		handleKafkaStreams();
//		Gson gson = new Gson();
		// Start chỗ này để push dữ liệu dạng key-value vào topic kafka
//		Properties propsKeyValue = new Properties();
//        propsKeyValue.put("bootstrap.servers", "localhost:9092");
//        propsKeyValue.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//        propsKeyValue.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//		KafkaProducer<String, String> producer = new KafkaProducer<>(propsKeyValue);
//		producer.send(new ProducerRecord<>("input-topic", "ManhND99", "Nguyen Dang Manh"));
//		producer.close();
		// End chỗ này để push dữ liệu dạng key-value vào topic kafka
		final User user = authenticate(authenticationRequest);
		if (user == null) {
			logger.error("JwtRequest body: {}", "User null");
		}
		final String token = jwtTokenUtil.generateToken(user);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	
	
	
	@SuppressWarnings("deprecation")
	private void handleKafkaStreams() {
		Properties props = new Properties();
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, "kafka-streams-app"); // Phải là duy nhất cho mỗi ứng dụng Kafka Streams
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
//		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(StreamsConfig.STATE_DIR_CONFIG, "M:\\tmp\\kakfa-streams\\manhnd99"); // Đường dẫn lưu trữ state
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // Đảm bảo tiêu thụ từ đầu topic nếu chưa có offset
		props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 1);
		props.put("group.id", "group-test"); 
		
		
//		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
//		consumer.subscribe(Collections.singletonList("input-topic"));
//
//		while (true) {
//		    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
//		    for (ConsumerRecord<String, String> record : records) {
//		        System.out.println("Consumed Record: Key = " + record.key() + ", Value = " + record.value());
//		    }
//		}
		
		
		StreamsBuilder builder = new StreamsBuilder();
		// Start chỗ này để tạo một state store tùy chỉnh trong kafka và lưu trữ dưới dạng key-value
		StoreBuilder<KeyValueStore<String, String>> storeBuilder =
			    Stores.keyValueStoreBuilder(
			        Stores.persistentKeyValueStore("manhnd99"),
			        Serdes.String(),
			        Serdes.String()
			    );
		// End chỗ này để tạo một state store tùy chỉnh trong kafka và lưu trữ dưới dạng key-value
		builder.addStateStore(storeBuilder);
		KStream<String, String> input = builder.stream("input-topic", Consumed.with(Serdes.String(), Serdes.String()));
		input.peek((key, value) -> {
		    System.out.println("Consumed Record: Key = " + key + ", Value = " + value);
		});
		KStream<String, String> filter = input.filter((key,value) -> !value.isEmpty()).mapValues((ValueMapper<String, String>) String::toUpperCase);
		filter.peek((key, value) -> {
		    System.out.println("Filtered Key: " + key + ", Filtered Value: " + value);
		});
		KStream<String, String> transformedStream = filter.transformValues(
			    new ValueTransformerSupplier<String, String>() {
			        @Override
			        public ValueTransformer<String, String> get() {
			            return new StateStoreTransformer();
			        }
			    },
			    "manhnd99"
			);
		 
		transformedStream.to("output-topic");
		KafkaStreams streams = new KafkaStreams(builder.build(), props);
		streams.start();
	}

	public class StateStoreTransformer implements ValueTransformer<String, String> {
	    private KeyValueStore<String, String> stateStore;
	    private String storeKey = "key"; // Đây là key cố định, bạn có thể thay đổi bằng cách sử dụng key của stream hoặc dữ liệu khác

	    @Override
	    public void init(ProcessorContext context) {
	        // Lấy state store từ context và kiểm tra
	        this.stateStore = (KeyValueStore<String, String>) context.getStateStore("manhnd99");
	        if (stateStore == null) {
	            throw new IllegalStateException("State store 'manhnd99' not found!");
	        }
	    }

	    @Override
	    public String transform(String value) {
	        // Đảm bảo key không cố định nếu cần
	        String key = "dynamic-key"; // Có thể sử dụng key của stream hoặc giá trị khác từ input stream

	        // Lưu dữ liệu vào state store
	        stateStore.put(key, value);

	        // Lấy lại dữ liệu từ state store bằng key vừa lưu
	        String storedValue = stateStore.get(key);

	        System.out.println("Stored Key: " + key + ", Stored Value: " + storedValue); // In giá trị từ store ra

	        return storedValue;  // Trả về giá trị đã lưu trong store
	    }

	    @Override
	    public void close() {
	        // Không cần thực hiện gì trong trường hợp này
	    }
	}


	
	private User authenticate(JwtRequest request) throws Exception {
		User user = null;
		
		try {
			Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
			user = (User)auth.getPrincipal();
		} catch (DisabledException e) {
			logger.error("USER_DISABLED", e);
//			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			logger.error("INVALID_CREDENTIALS", e);
//			throw new Exception("INVALID_CREDENTIALS", e);
		}
		
		return user;
	}

	@GetMapping(value = "/find-all") 
	public ResponseEntity<?> getAllUsers(@RequestParam(defaultValue = "0") Integer pageNo,
										 @RequestParam(defaultValue = "10") Integer pageSize,
										 @RequestParam(name = "username", required = false) String username
			) {
		Pageable pageable = PageRequest.of(pageNo, pageSize);
		return ResponseEntity.ok().body(userService.getAllUsers(username, pageable));
	}
	
	@PostMapping(value = "/save")
	public ResponseEntity<?> saveUsers(@RequestBody User users) {
		userService.saveUsers(users);
		return ResponseEntity.ok().body(HttpStatus.CREATED);
	}
}
