package com.manhnd.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manhnd.userservice.model.Information;
import com.manhnd.userservice.service.InformationService;

@RestController
@RequestMapping("/api/v1/informations")
public class InformationController {

	@Autowired
	private InformationService informationService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<?> save(@RequestBody List<Information> listInfor) {
		informationService.saveInformation(listInfor);
		return ResponseEntity.ok().body(HttpStatus.OK);
	}
	
	@GetMapping("/")
	public ResponseEntity<?> get() {
		informationService.getDataOtherSystem();
		return ResponseEntity.ok().body(HttpStatus.OK);
	}
}
