package com.manhnd.userservice.controller;

import java.util.List;

import org.apache.catalina.authenticator.SpnegoAuthenticator.AuthenticateAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.manhnd.userservice.model.Profile;
import com.manhnd.userservice.service.ProfileService;

@RequestMapping("/api/v1/profiles")
@RestController
public class ProfileController {

	@Autowired
	private ProfileService profileService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<?> saveProfiles(@RequestBody List<Profile> listProfiles) {
		profileService.saveProfiles(listProfiles);
	return ResponseEntity.ok().body(HttpStatus.OK);
	}
	
	@GetMapping(value = "/getProfile")
	public ResponseEntity<?> getProfiles(@RequestParam(defaultValue = "1") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam String id) {
		AuthenticateAction authenticateAction = (AuthenticateAction) SecurityContextHolder.getContext().getAuthentication();
		System.out.println(authenticateAction);
		Pageable pageable = PageRequest.of(pageNo, pageSize);
		return ResponseEntity.ok().body(profileService.getProfile(id, pageable));
	}
	
	@GetMapping
	public ResponseEntity<?> getAllProfiles() {
		return ResponseEntity.ok().body(profileService.getAllProfiles());
	}
}
