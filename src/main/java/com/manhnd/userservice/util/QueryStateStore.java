package com.manhnd.userservice.util;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.StoreQueryParameters;

public class QueryStateStore {
    private final KafkaStreams streams;

    public QueryStateStore(KafkaStreams streams) {
        this.streams = streams;
    }

    public String getValueFromStore(String key) {
        ReadOnlyKeyValueStore<String, String> store = streams.store(
        	    StoreQueryParameters.fromNameAndType("data-test", QueryableStoreTypes.keyValueStore())
        	);
        return store.get(key);
    }
}

