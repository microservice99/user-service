package com.manhnd.userservice.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@ConditionalOnBean(RedisTemplate.class)
@Slf4j
@SuppressWarnings({"unchecked"})
public class RedisHelper {
    private final RedisTemplate<Object, Object> redisTemplate;
    
    private static final String QUEUE_KEY = "queueKey";
    
    @Autowired
    public RedisHelper(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void enqueue(String queueName, Object value) {
        redisTemplate.opsForList().rightPush(queueName, value);
    }

    public void enqueueAll(String queueName, Collection<Object> values) {
        redisTemplate.opsForList().rightPushAll(queueName, values);
    }

    public <T> T dequeue(String queueName, Class<T> clazz) {
        return (T) redisTemplate.opsForList().leftPop(queueName);
    }

    public <T> List<T> dequeueAll(String queueName, long count, Class<T> clazz) {
        return (List<T>) redisTemplate.opsForList().leftPop(queueName, count);
    }

    public <T> List<T> dequeueAll(String queueName, Class<T> clazz) {
        Long count = redisTemplate.opsForList().size(queueName);
        if (count == null) {
            return new ArrayList<>();
        }

        return (List<T>) redisTemplate.opsForList().leftPop(queueName, count);
    }

    public void put(String mapName, String mapKey, Object value) {
        redisTemplate.opsForHash().put(mapName, mapKey, value);
    }

    public <T> T get(String mapName, String mapKey) {
        return (T) redisTemplate.opsForHash().get(mapName, mapKey);
    }

    public <T> T getAndRemove(String mapName, String mapKey, Class<T> clazz) {
        T value = (T) redisTemplate.opsForHash().get(mapName, mapKey);
        redisTemplate.opsForHash().delete(mapName, mapKey);

        return value;
    }
    
    public void setKey(String key, Object value) {
    	redisTemplate.opsForValue().set(key, value);
    }
    
    public void setKeyTest(String key, String str) {
    	redisTemplate.opsForValue().set(key, str);
    }
    
    public Object getKey(String key) {
        return redisTemplate.opsForValue().get(key);
    }
    
    public void pushToQueue(Object value) {
    	redisTemplate.opsForList().rightPush(QUEUE_KEY, value);
    }
    
    public Object popFromQueue() {
    	return redisTemplate.opsForList().leftPop(QUEUE_KEY);
    }
    
    public Object pushLeftPushQueue(String keyString,Object value) {
    	return redisTemplate.opsForList().leftPush(keyString, value);
    }
}
