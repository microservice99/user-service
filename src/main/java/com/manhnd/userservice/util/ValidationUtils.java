package com.manhnd.userservice.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Author: PhucVM
 * Date: 24/07/2022
 */
public class ValidationUtils {
    
    public static boolean isNullOrEmpty(String st) {
        return st == null || st.isEmpty();
    }
    
    public static boolean isNullOrEmpty(Object obj) {
        return obj == null || obj.toString().isEmpty();
    }
    
    public static boolean isNullOrEmpty(List<?> lst) {
        return lst == null || lst.isEmpty();
    }
    
    public static boolean isNullOrEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }
    
    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
    
    public static boolean isUUIDString(String input) {
        Pattern pattern = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
        return pattern.matcher(input).matches();
    }
    public static boolean isNumeric(String strNum){
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
