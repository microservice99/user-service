package com.manhnd.userservice.util;

import java.util.Collection;

import org.springframework.data.domain.Pageable;

import lombok.NoArgsConstructor;
@NoArgsConstructor
public class DataUtil {
	
    public static int getPage(Pageable pageable) {
        if (pageable.getPageNumber() <= 0)
            return 0;
        else return (pageable.getPageNumber() - 1) * pageable.getPageSize();
    }

    public static int getPageSize(int pageable, int size) {
        if (pageable <= 0)
            return 0;
        else return (pageable - 1) * size;
    }
    
    public static boolean isNullOrEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
    
    public static int safeToInt(Object obj1, Integer defaultValue) {
        if (obj1 == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(obj1.toString());
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }
    
    public static int safeToInt(Object obj1) {
        return safeToInt(obj1, 0);
    }

    /**
     * @param obj1 Object
     * @return String
     */
    public static String safeToString(Object obj1, String defaultValue) {
        if (obj1 == null) {
            return defaultValue;
        }

        return obj1.toString();
    }
    
    public static String safeToString(Object obj1) {
        return safeToString(obj1, "");
    }
}
