package com.manhnd.userservice.util;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.SerializationUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import lombok.extern.slf4j.Slf4j;

/**
 * Author: PhucVM
 * Date: 10/08/2022
 */
@Slf4j
@SuppressWarnings({"unchecked"})
public class ObjectUtils {
    
    private static final ObjectMapper OM = new ObjectMapper()
            .enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS)
            .registerModule(new ParameterNamesModule())
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
    
    public static String toJson(Object object) {
        try {
            return OM.writeValueAsString(object);
        } catch (Exception e) {
            return "";
        }
    }
    
    public static <T> T toObject(String json, Class<T> clazz) {
        try {
            return OM.readValue(json, clazz);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static <T> T toObject(String json, TypeReference<T> type) {
        try {
            return OM.readValue(json, type);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static <T> T convertObject(Object fromObject, Class<T> toClass) {
        try {
            return OM.convertValue(fromObject, toClass);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static <T> T convertObject(Object fromObject, TypeReference<T> toType) {
        try {
            return OM.convertValue(fromObject, toType);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static String writeValueAsString(Object value) {
        try {
            return OM.writeValueAsString(value);
        } catch (Exception e) {
            return null;
        }
    }

    public static Object readValue(String value) {
        try {
            return OM.readValue(value, Object.class);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static boolean isValidJSON(String json) {
        try {
            OM.readTree(json);
        } catch (Exception ex) {
            return false;
        }
    
        return true;
    }
    
    /**
     * Deep copy
     */
    public static <T extends Serializable> T clone(T object) {
        return SerializationUtils.clone(object);
    }
    
    public static String jsonArrayToString(String jsonArray, String delimiter) {
        try {
            List<Object> strings = OM.readValue(jsonArray, List.class);
            return strings.stream().map(String::valueOf).collect(Collectors.joining(delimiter));
        } catch (Exception e) {
            return jsonArray;
        }
    }
    
    public static String jsonArrayToString(List<?> jsonArray, String delimiter) {
        if (!ValidationUtils.isNullOrEmpty(jsonArray)) {
            return jsonArray.stream().map(String::valueOf).collect(Collectors.joining(delimiter));
        }
        
        return jsonArray != null ? jsonArray.toString() : "";
    }
    
    public static String minifyJson(String json) {
        String regex = "((?<![\\w}])\\s(?=[\\s\":]*))|(\\s+(?=\"))";
        try {
            if (!ValidationUtils.isNullOrEmpty(json)) {
                Pattern pattern = Pattern.compile(regex, Pattern.UNICODE_CASE | Pattern.MULTILINE | Pattern.UNICODE_CHARACTER_CLASS);
                Matcher matcher = pattern.matcher(json);
                return matcher.replaceAll("");
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        
        return json;
    }
    
    public static <T> T jsonToObject(String json, TypeReference<T> type) {
    	try {
        	ObjectMapper mapper = new ObjectMapper();
        	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        	mapper.setSerializationInclusion(Include.NON_NULL);
        	return mapper.readValue(json, type);
		} catch (Exception e) {
			return null;
		}
    }
    
    public static String objectToJson(Object object) {
        try {
        	ObjectMapper mapper = new ObjectMapper();
        	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            return "";
        }
    }
}
