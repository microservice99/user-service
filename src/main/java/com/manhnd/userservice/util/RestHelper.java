package com.manhnd.userservice.util;


import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RestHelper {

	final String TRANS_ID = "request_id";
	
	@Autowired
	private RestTemplate restTemplate;

	public ResponseEntity<?> send(String serviceUrl, String path, String token, HttpMethod method, Class<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(token, null), responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(token, null), responseType);
		}
	}

	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, Class<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, null), responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, null), responseType);
		}
	}

	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, Class<?> responseType, Map<String, ?> uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null) {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, null), responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, null), responseType);
		}
	}

	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, Object body, Class<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, body), responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, body), responseType);
		}
	}

	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, ParameterizedTypeReference<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, null), responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, null), responseType);
		}
	}

	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, Object body, ParameterizedTypeReference<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, body), responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, buildInnerHttpEntity(null, body), responseType);
		}
	}


	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, HttpEntity<?> httpEntity, Class<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, httpEntity, responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, httpEntity, responseType);
		}
	}

	public ResponseEntity<?> send(String serviceUrl, String path, HttpMethod method, HttpEntity<?> httpEntity, ParameterizedTypeReference<?> responseType, Object... uriVariables) {
		String url = serviceUrl + path;
		if (uriVariables != null && uriVariables.length > 0) {
			return restTemplate.exchange(url, method, httpEntity, responseType, uriVariables);
		} else {
			return restTemplate.exchange(url, method, httpEntity, responseType);
		}
	}

	private HttpEntity<?> buildInnerHttpEntity(String token, Object body) {
		HttpHeaders headers = initHeaders();
		if (StringUtils.isNotBlank(token)) {
			headers.add("Authorization", "Bearer " + token);
		}

		if (body != null) {
			return new HttpEntity<>(body, headers);
		} else {
			return new HttpEntity<>(headers);
		}
	}

	public HttpHeaders initHeaders() {
		HttpHeaders initHeaders = new HttpHeaders();
	    initHeaders.setContentType(new MediaType(MediaType.APPLICATION_JSON, StandardCharsets.UTF_8));
	    initHeaders.setAccept(Collections.singletonList(new MediaType(MediaType.APPLICATION_JSON, StandardCharsets.UTF_8)));

	    return initHeaders;
	}

	public HttpEntity<?> buildHttpEntity(HttpHeaders headers, Object body) {
		return buildHttpEntity(headers, null, body);
	}

	public HttpEntity<?> buildHttpEntity(HttpHeaders headers, String token, Object body) {
		if (StringUtils.isNotBlank(token)) {
			headers.add("Authorization", "Bearer " + token);
		}

		if (body != null) {
			return new HttpEntity<>(body, headers);
		} else {
			return new HttpEntity<>(headers);
		}
	}

	public String buildUrl(String hostUrl, String apiPath, Map<String, ?> uriVariables, Map<String, ?> requestParams) {
		if (ValidationUtils.isNullOrEmpty(hostUrl)) {
			return null;
		}

		String pathSeparator = "/";
		String result;

		if (!apiPath.startsWith(pathSeparator)) {
			apiPath = pathSeparator + apiPath;
		}
		result = hostUrl + apiPath;
		if (result.endsWith(pathSeparator)) {
			result = result.substring(0, result.lastIndexOf(pathSeparator));
		}

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(result);

		if (!ValidationUtils.isNullOrEmpty(requestParams)) {
			for (String key : requestParams.keySet()) {
				builder.queryParam(key, requestParams.get(key));
			}
		}

		if (uriVariables != null && !uriVariables.isEmpty()) {
			result = builder.buildAndExpand(uriVariables).normalize().toUriString();
		} else {
			result = builder.build().normalize().toUriString();
		}

		return result;
	}

	public <T> ResponseEntity<T> get(String url, HttpHeaders headers, ParameterizedTypeReference<T> type) throws Exception {
		return exchange(url, HttpMethod.GET, headers, null, type);
	}

	public <T> ResponseEntity<T> get(String url, HttpHeaders headers, ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) throws Exception {
		return exchange(url, HttpMethod.GET, headers, null, type, uriVariables);
	}

	public <T> T getAndGetBody(String url, HttpHeaders headers, ParameterizedTypeReference<T> type) throws Exception {
		return exchangeAndGetBody(url, HttpMethod.GET, headers, null, type, null);
	}

	public <T> T getAndGetBody(String url, HttpHeaders headers, ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) throws Exception {
		return exchangeAndGetBody(url, HttpMethod.GET, headers, null, type, uriVariables);
	}

	public <T> ResponseEntity<T> post(String url, HttpHeaders headers, Object requestBody, ParameterizedTypeReference<T> type) throws Exception {
		return exchange(url, HttpMethod.POST, headers, requestBody, type);
	}

	public <T> ResponseEntity<T> post(String url, HttpHeaders headers, Object requestBody, ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) throws Exception {
		return exchange(url, HttpMethod.POST, headers, requestBody, type, uriVariables);
	}

	public <T> T postAndGetBody(String url, HttpHeaders headers, Object requestBody, ParameterizedTypeReference<T> type) throws Exception {
		return exchangeAndGetBody(url, HttpMethod.POST, headers, requestBody, type, null);
	}

	public <T> T postAndGetBody(String url, HttpHeaders headers, Object requestBody, ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) throws Exception {
		return exchangeAndGetBody(url, HttpMethod.POST, headers, requestBody, type, uriVariables);
	}

	public <T> T exchangeAndGetBody(String url, HttpMethod method, HttpHeaders headers, Object requestBody, final ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) throws Exception {
		ResponseEntity<T> response = exchange(url, method, headers, requestBody, type, uriVariables);

		return getResponseBody(response);
	}

	public <T> T exchangeAndGetBody(String url, HttpMethod method, HttpEntity<?> entity, final ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) {
		ResponseEntity<T> response = exchange(url, method, entity, type, uriVariables);

		return getResponseBody(response);
	}

	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpHeaders headers, Object requestBody, final ParameterizedTypeReference<T> type) throws Exception {
		return exchange(url, method, headers, requestBody, type, null);
	}

	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpHeaders headers, Object requestBody, final ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) throws Exception {
		if (ValidationUtils.isNullOrEmpty(headers)) {
			headers = new HttpHeaders();
		}

		if (!headers.containsKey(HttpHeaders.CONTENT_TYPE)) {
			headers.setContentType(MediaType.APPLICATION_JSON);
		}
		if (!headers.containsKey(TRANS_ID)) {
			String reqId = null;
			if (ValidationUtils.isNullOrEmpty(MDC.get(TRANS_ID))) {
				reqId = UUID.randomUUID().toString();
			} else {
				reqId = MDC.get(TRANS_ID);
			}
			headers.put(TRANS_ID, Arrays.asList(reqId));
		}

		// if request body is null => set default empty string array to avoid error
		requestBody = (requestBody == null) ? new String[]{} : requestBody;
		HttpEntity<?> entity = hasRequestBody(method) ? new HttpEntity<>(requestBody, headers) : new HttpEntity<>(headers);

		return exchange(url, method, entity, type, uriVariables);
	}

	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> entity, final ParameterizedTypeReference<T> type, Map<String, ?> uriVariables) {
		long startTime = System.currentTimeMillis();

		ResponseEntity<T> responseEntity = null;
		try {
			if (uriVariables != null) {
				responseEntity = restTemplate.exchange(url, method, entity, type, uriVariables);
			} else {
				responseEntity = restTemplate.exchange(url, method, entity, type);
			}
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw ex;
		} finally {
			long endTime = System.currentTimeMillis() - startTime;
			if(endTime >= 1000) {
				log.info("Starting call service {}", url);
				log.info("Request URL: {}", url);
				log.info("Request headers: {}", entity != null ? entity.getHeaders() : null);
				log.info("Request method: {}", method.toString());
				log.info("Request body: {}", entity != null ? ObjectUtils.toJson(entity.getBody()) : null);
				log.info("Request Variables: {}", uriVariables != null ? ObjectUtils.toJson(uriVariables) : null);
				log.info("Response status: {}", responseEntity != null ? responseEntity.getStatusCode() : null);
				log.info("Finished call service {} in {} ms", url, endTime);
			}
		}

		return responseEntity;
	}

	public <T> T getResponseBody(ResponseEntity<T> responseEntity) {
		T responseBody = null;
		if (responseEntity != null) {
			responseBody = responseEntity.getBody();
		}

		return responseBody;
	}

	public boolean hasRequestBody(HttpMethod httpMethod) {
		return !httpMethod.equals(HttpMethod.GET)
				&& !httpMethod.equals(HttpMethod.DELETE)
				&& !httpMethod.equals(HttpMethod.TRACE)
				&& !httpMethod.equals(HttpMethod.OPTIONS)
				&& !httpMethod.equals(HttpMethod.HEAD);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getAndGetBodyService(String url, HttpHeaders headers) throws Exception {
		Map<String, Object> responseBody = null;
		String body = "";
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            // Tạo HttpGet với URL và header
            HttpGet request = new HttpGet(url);
            Header header = new BasicHeader("Authorization", headers.get("Authorization").get(0));
            request.setHeader(header);
            
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                System.out.println("Response code: " + response.getStatusLine().getStatusCode());
                if(response.getStatusLine().getStatusCode() == 200) {
                	org.apache.http.HttpEntity entity = response.getEntity();
                    body = EntityUtils.toString(entity);
                }
            }

        }
		if(StringUtils.isNotEmpty(body)) {
			ObjectMapper objectMapper = new ObjectMapper();
	        responseBody = objectMapper.readValue(body, Map.class);
		}
		
		return responseBody;
	}
}
