package com.manhnd.userservice.util;

import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.UUID;

import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;

public class StateStoreTransformer implements ValueTransformer<String, String> {

    private KeyValueStore<String, String> stateStore;

    @Override
    public void init(ProcessorContext context) {
        // Lấy state store đã được khai báo
        this.stateStore = (KeyValueStore<String, String>) context.getStateStore("state-store");
    }

    @Override
    public String transform(String value) {
        // Lưu dữ liệu vào state store
        stateStore.put(UUID.randomUUID().toString(), value);

        // Lấy dữ liệu từ state store
        String storedValue = stateStore.get("key");

        System.out.println("Key: key, Value: " + storedValue);

        return storedValue;
    }

    @Override
    public void close() {
        // Không cần thực hiện gì trong trường hợp này
    }
}
