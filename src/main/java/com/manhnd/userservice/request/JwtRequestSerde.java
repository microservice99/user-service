package com.manhnd.userservice.request;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JwtRequestSerde implements Serde<JwtRequest>{
	
	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public Serializer<JwtRequest> serializer() {
		return (topic, data) -> {
            try {
                return objectMapper.writeValueAsBytes(data);
            } catch (Exception e) {
                throw new RuntimeException("Failed to serialize JwtRequest", e);
            }
        };
	}

	@Override
	public Deserializer<JwtRequest> deserializer() {
		return (topic, data) -> {
            try {
                return objectMapper.readValue(data, JwtRequest.class);
            } catch (Exception e) {
                throw new RuntimeException("Failed to deserialize JwtRequest", e);
            }
        };
	}

}
