package com.manhnd.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.manhnd.userservice.model.Information;

public interface InformationRepository extends JpaRepository<Information, String>{

}
