package com.manhnd.userservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.manhnd.userservice.model.User;


public interface UserRepository extends PagingAndSortingRepository<User, String>{

	@Query(value = "SELECT * FROM users where username = ?", nativeQuery = true)
	User findByUsername(String username);

	@Query(value = "SELECT * FROM users WHERE accesstoken = ?", nativeQuery = true)
	User findUserByToken(String accessToken);

	@Query(value = "SELECT * FROM users WHERE refreshtoken = ?", nativeQuery = true)
	User findUserByRefreshToken(String refreshToken);

	@Query(value = "SELECT * FROM users WHERE ids =?", nativeQuery = true)
	User findUserByIds(String ids);

	@Query(value = "UPDATE users SET refreshtoken = NULL WHERE ids =?", nativeQuery = true)
	void deleteRefreshToken(String ids);
	
	@Query(value = "SELECT * FROM users WHERE username like %:username%", nativeQuery = true)
	Page<User> search(@Param("username") String username, Pageable pageable);
	
}
