package com.manhnd.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.manhnd.userservice.model.Profile;

public interface ProfileRepository extends JpaRepository<Profile, String>{

}
